package com.company;

import com.company.school.Employee;
import com.company.school.Student;
import com.company.school.Teacher;
import com.company.shapes.Rectangle;
import com.company.shapes.Square;
import com.company.shapes.Triangle;

public class Main {

    public static void main(String[] args) {

        Student s1 = new Student(5012203330033l, "Ion", "Popescu", 21, 9d);
        Teacher t1 = new Teacher(5052203330033l, "Maria", "Ionesc", 24, "matematica");
        Employee e1 = new Employee(6022203330033l, "Andrei", "Pop", 27, "contabil");

        System.out.println(s1.toString());
        System.out.println(t1.toString());
        System.out.println(e1.toString());

        Triangle triangle = new Triangle(2, "White", 2);
        Square square = new Square(2, "Black");
        Rectangle rectangle = new Rectangle(4, "blue", 3);

        System.out.println(triangle.getArea());
        System.out.println(square.getArea());
        System.out.println(rectangle.getArea());
        System.out.println(triangle);


    }
}

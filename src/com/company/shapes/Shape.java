package com.company.shapes;

public class Shape {
    private double base;
    String color;

    public Shape(){

    }
    public Shape(double base,String color){
        this.base = base;
        this.color = color;

    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public String toString() {
        return "Shape{" +
                "base=" + base +
                ", color='" + color + '\'' +
                '}';
    }
}

package com.company.shapes;

public class Triangle extends Shape{
    private double height;

    public Triangle(){

    }
    public Triangle(double base,String color, double height){
        super(base,color);
        this.height = height;
    }

    public double getArea(){
        return getBase()*height/2;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "color='" + color + '\'' +
                ", height=" + height +
                '}';
    }
}

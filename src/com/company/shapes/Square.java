package com.company.shapes;

public class Square extends Shape {
    public Square() {

    }

    public Square(double base,String color) {
        super(base,color);
    }

    public double getArea() {
        return getBase() * getBase();
    }

}

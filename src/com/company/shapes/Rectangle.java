package com.company.shapes;

public class Rectangle extends Shape{
    private double height;

    public Rectangle(){
    }
    public Rectangle(double base, String color,double height){
        super(base,color);
        this.height = height;
    }

    public double getArea(){
        return getBase() * height;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }
}

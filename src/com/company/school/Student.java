package com.company.school;

//Student is a Person -> Student extends Person
public class Student extends Person {
    private double examGrade;

    public Student() {

    }

    public Student(long cnp, String firstName, String lastName, int age, double examGrade) {
        //super apeleaza constructorul Clasei mostenite Person
        super(cnp, firstName, lastName, age);
        this.examGrade = examGrade;

    }

    public double getExamGrade() {
        return examGrade;
    }

    public void setExamGrade(double examGrade) {
        this.examGrade = examGrade;
    }

    @Override
    public String toString(){
        return super.toString() +" Nota mea la examen este " + examGrade + ".";

    }

}

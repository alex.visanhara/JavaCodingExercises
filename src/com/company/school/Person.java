package com.company.school;

public class Person {
    private long cnp;
    private String firstName;
    private String lastName;
    private int age;

    public Person() {

    }

    public Person(long cnp, String firstName, String lastName, int age) {
        this.cnp = cnp;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    // Setters
    public void setCnp(long cnp) {
        this.cnp = cnp;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    //Getters
    public long getCnp() {
        return cnp;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }


    public String toString() {
        return "Salut! Eu sunt " + firstName + " " + lastName + ", am " + age + " ani.";
    }
}

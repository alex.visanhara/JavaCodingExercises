package com.company.school;

public class Teacher extends Person {
    private String subject;

    public Teacher() {

    }

    public Teacher(long cnp, String firstName, String lastName, int age, String subject) {
        super(cnp, firstName, lastName, age);
        this.subject = subject;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Override
    public String toString() {
        return super.toString() + " Sunt profesor pe subiectul " + subject + ".";

    }
}

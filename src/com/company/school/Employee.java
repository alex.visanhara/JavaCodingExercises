package com.company.school;

public class Employee extends Person {
    private String occupation;

    public Employee() {

    }

    public Employee(long cnp, String firstName, String lastName, int age, String occupation) {
        super(cnp, firstName, lastName, age);
        this.occupation = occupation;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    @Override
    public String toString() {
        return super.toString() + " Sunt angajat ca " + occupation + ".";

    }
}
